<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    	<link href="top.css"  rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<title>投稿</title>
<script type="text/javascript">

$(function () {
	  $("textarea").keyup(function(){
	    var counter = $(this).val().length;
	    $("#countUp").text(counter);

	    if(counter == 0){
	      $("#countUp").text("1000文字を超えたら赤色になります");
	    }
	    if(counter >= 1000){
	      $("#countUp").css("color","red");
	    } else {
	      $("#countUp").css("color","#666");
	    }
	  });
	});
</script>
</head>
<body>
<h2>投稿</h2>
	<c:choose>
		<c:when test="${ empty loginUser }">
ログインしてください
<br>
				<a href="login">ログイン</a>
</c:when>
		<c:when test="${ not empty loginUser }">
	<c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
	<div class="form-area">
        <form action="newMessage" method="post">
            <br />
            <label for="categoly">カテゴリー</label><input name="categoly" id="categoly" placeholder ="10文字以内で入力"/>
            <br />
            <label for="title">タイトル</label><input name="title" id="title"  placeholder ="30文字以内で入力"/>
            <br />
            <textarea name="text" cols="100" rows="5" class=text-box placeholder ="1000文字以内で入力"></textarea>
            <p id="countUp">0</p>
            <input type="submit" value="投稿">
        </form>
          <a href="./">戻る</a>
	</div>
	</c:when>
	</c:choose>
</body>
</html>