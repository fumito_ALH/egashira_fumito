<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<% %>
<!DOCTYPE html">
<html>
<head>
<meta charset="UTF-8">
<link href="top.css" rel="stylesheet" type="text/css">
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<title>ユーザー管理画面</title>

<script>
function TT(id, isStopped){
	var str;
	if(isStopped == 0){
		str = "停止"
	}else{
		str = "復活";
	}
	if(window.confirm('ユーザーを' + str + 'します。よろしいですか？')){
		window.location.href = "UserManagement?id=" + id;
	}
	else{
		window.alert('キャンセルされました');
	}
}

$(function(){
    $("tr:even").addClass("odd");
});
</script>
</head>
<body>
<h2>ユーザー管理</h2>
	<c:choose>
		<c:when test="${ empty loginUser }">
ログインしてください
<br>
				<a href="login">ログイン</a>
	</c:when>
					</c:choose>
		<c:choose>
		<c:when test="${loginUser.position == 1 && loginUser.blanch == 1}">
			<a href="index.jsp">戻る</a>
			<br>
			<a href="signup">新規登録</a>
			<table border="1" align="center">
				<thead>
					<tr>
						<th>login_id</th>
						<th>name</th>
						<th>blanch</th>
						<th>position</th>
						<th>id</th>
						<th>created_date</th>
						<th>updated_date</th>
						<th>edit</th>
						<th>account</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${userList}" var="item">
						<tr>
							<td><c:out value="${item.login_id}" /></td>
							<td><c:out value="${item.name}" /></td>
							<td><c:out value="${item.blanchName}" /></td>
							<td><c:out value="${item.positionName}" /></td>
							<td><c:out value="${item.id}" /></td>
							<td><c:out value="${item.createdDate}" /></td>
							<td><c:out value="${item.updatedDate}" /></td>
							<td><a href="setting?id=${item.id}">edit</a></td>
							<td><button onClick="TT(${item.id }, ${item.is_stopped})">${ item.is_stopped == 1 ? "停止中" : "稼働中" }</button></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:when>
		<c:when test="${ not empty loginUser }">
		権限がありません
		<a href="index.jsp">戻る</a>
				</c:when>
		</c:choose>
</body>
</html>