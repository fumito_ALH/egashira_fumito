<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ログイン</title>
        <link href="./top.css" rel="stylesheet" type="text/css" >
    </head>
    <body>
    <h2>ログイン</h2>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

            <form action="login" method="post">
            	<br />
                <label for="login_id">LOGIN ID</label>
                <input name="login_id" id="login_id" />
                <br />

                <label for="pass">パスワード</label>
                <input name="pass" type="password" id="pass"/>
                <br />

                <input type="submit" value="ログイン" />
                <br />
                <a href="./">戻る</a>
            </form>
            <div class="copyright"> Copyright(c)egashira</div>
        </div>
    </body>
</html>