<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="top.css" rel="stylesheet" type="text/css">
    <title>ユーザー登録</title>
    </head>
    <body>
        <h2>ユーザー登録</h2>
	<c:choose>
		<c:when test="${ empty loginUser }">
			ログインしてください
			<br>
				<a href="login">ログイン</a>
		</c:when>
			</c:choose>
		<c:choose>
		<c:when test="${loginUser.position == 1 && loginUser.blanch == 1}">
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            <form action="signup" method="post">
                <br />
               	<label for="name">名前</label><input name="name" id="name"  placeholder ="10文字以内"/>
                <br />
				<label for="pass">パスワード</label> <input name="pass" type="password" id="pass"  placeholder ="6文字以上20文字以内""/>
                <br />
                <label for="repass">確認用パスワード</label> <input name="repass" type="password" id="repass"  placeholder ="6文字以上20文字以内"/>
                <br />
                <label for="login_id">ログインID</label> <input name="login_id" id="login_id"  placeholder ="6文字以上20文字以内""/>
                <br />
                <label for="blanch">支店名</label>
                 <select name="blanch">
					 <c:forEach items="${blanch}" var="bl">
            			<option value="${bl.id}"> ${bl.blanch_name}</option>
        			 </c:forEach>
			  	</select>
			  	<br />
			  	<label for="position">部署・役職</label>
			  	 <select name="position">
					 <c:forEach items="${position}" var="position">
            			<option value="${position.id}"> ${position.position_name}</option>
        			 </c:forEach>
				  </select>
				  <br />
                <input type="submit" value="登録" />
                <br /> <a href="./">戻る</a>
            </form>
            <div class="copyright">Copyright(c)Your Name</div>
        </div>
        </c:when>
        <c:when test="${ not empty loginUser }">
		権限がありません
		<a href="index.jsp">戻る</a>
		</c:when>
        </c:choose>
    </body>
</html>