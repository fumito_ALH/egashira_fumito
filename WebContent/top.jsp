<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="top.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<title>掲示板</title>

<script>
function Mdelete(id){
	if(window.confirm('メッセージを削除します。よろしいですか？')){
		window.location.href = "MessageDelete?messageDelete=" + id;
	}
	else{
		window.alert('キャンセルされました');
	}
}

function Cdelete(id){
	if(window.confirm('コメントを削除します。よろしいですか？')){
		window.location.href = "CommentDelete?commentDelete=" + id;
	}
	else{
		window.alert('キャンセルされました');
	}
}
$(function(){	//クリックしたら画面上まで戻る
	  $('.toTop').click(function () {
	    $('body,html').animate({
	      scrollTop: 0
	    }, 800);
	    return false;
	  });
	});
</script>
</head>
<body>
	<h2>ホーム</h2>
	<div class="main-contents">
		<div class="header">

			<c:if test="${ empty loginUser }">
			ログインしてください

			<br>
				<a href="login">ログイン</a>
			</c:if>
			<c:if test="${ not empty loginUser }">
				<a href="logout">ログアウト</a>
				<a href="newMessage">投稿</a>

				<c:choose>
					<c:when test="${loginUser.position == 1 && loginUser.blanch == 1}">
						<a href="AllUser">ユーザー管理画面 </a>
					</c:when>
				</c:choose>
			</c:if>
		</div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<div class="search"></div>
		<c:if test="${ not empty loginUser }">
			<div class="profile">
				<div class="name">
					<h2>
						<c:out value="${loginUser.name}" />
					</h2>
				</div>
				<form action="CateegolySearch" method="get">
					<input name="search" id="search" placeholder="カテゴリーを入力">
					<button type="submit">検索</button>
				</form>
				<form action="SearchServlet" method="get">
					<input type="date" name="Sdate" id="Sdate"></input>
					~
					<input type="date" name="Edate" id="Edate"></input>
					<button type="submit">検索</button>
				</form>
				<div class="messages">
					<c:forEach items="${messages}" var="message">
						<div class="message">
							<div class="account-name">
							<div class = "name">
								<c:out value="${message.name}" />
							</div>
							<div class="categoly">
								カテゴリー ：
								<c:out value="${message.categoly}" />
							</div>
							<div class="title">
								タイトル ：
								<c:out value="${message.title}" />
							</div>
							<div class="text">
								テキスト ：
								<c:out value="${message.text}" />
							</div>
							<c:if test="${loginUser.id == message.userId}">
								<button onClick="Mdelete(${message.id })">削除</button>
							</c:if>
							<div class="date">
								<fmt:formatDate value="${message.created_date}"
									pattern="yyyy/MM/dd HH:mm:ss" />
							</div>
						</div>
						</div>
						<c:forEach items="${comment}" var="tweet">
							<c:if test="${tweet.message_id == message.id}">
								<div class="comment">
									<div class="text">
										本文:
										<c:out value="${tweet.text}" />
									</div>
									<div class="created_date">
										コメントした日時:
										<c:out value="${tweet.created_date}" />
									</div>
									<div class="name">
										コメントしたユーザー:
										<c:out value="${tweet.name}" />
									</div>
									<c:if test="${loginUser.id == tweet.user_id}">
										<button onClick="Cdelete(${tweet.id })">削除</button>
									</c:if>
								</div>
							</c:if>
						</c:forEach>
						<form action="NewComment" method="post">
							<input type="hidden" name="messages" id="messages"
								value="${message.id}" />
							<textarea name="text" cols="100" rows="5" class=text-box
								placeholder="コメントを入力"></textarea>
							<input type="submit" value="コメントする" />
						</form>
					</c:forEach>
				</div>
			</div>
		</c:if>
		<p class="toTop">ページトップへ</p>
	</div>
	<div class="copyright">Copyright(c)egashira</div>
</body>
</html>