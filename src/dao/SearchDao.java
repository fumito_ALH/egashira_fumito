
package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserMessage;
import exception.SQLRuntimeException;
public class SearchDao {
    public List<UserMessage> getUserMessages(Connection connection, Date searchDate ,Date searchAfterDate) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT * FROM messages "
            		+ "INNER JOIN users ON messages.user_id = users.id "
            		+ "WHERE messages.created_date BETWEEN ? AND ?; ");

            ps = connection.prepareStatement(sql.toString());
            ps.setDate(1, searchDate);
            ps.setDate(2, searchAfterDate);
            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<UserMessage> getUserMessages(Connection connection, String catee) {
    	catee = "%" + catee + "%";
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT * FROM messages "
            		+ "INNER JOIN users ON messages.user_id = users.id "
            		+ "WHERE categoly LIKE ?");

            ps = connection.prepareStatement(sql.toString());
            ps.setString(1, catee);
            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                String text = rs.getString("text");
                String categoly = rs.getString("categoly");
                int id = rs.getInt("id");
                String title = rs.getString("title");
                Timestamp createdDate = rs.getTimestamp("created_date");
                int userId = rs.getInt("user_id");
                String name = rs.getString("name");

                UserMessage message = new UserMessage();
                message.setText(text);
                message.setCategoly(categoly);
                message.setCreated_date(createdDate);
                message.setId(id);
                message.setTitle(title);
                message.setUserId(userId);
                message.setName(name);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}