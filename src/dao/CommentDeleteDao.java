package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentDeleteDao  {
	public void delete(Connection connection, Comment comment) {

	PreparedStatement ps = null;
    try {
        StringBuilder sql = new StringBuilder();
        sql.append("DELETE FROM comments WHERE id = ? ; ");
        ps = connection.prepareStatement(sql.toString());
        ps.setInt(1, comment.getId());

        ps.executeUpdate();
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
}
}
