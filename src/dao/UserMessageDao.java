package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserMessage;
import exception.SQLRuntimeException;
public class UserMessageDao {
    public List<UserMessage> getUserMessages(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.categoly as categoly, ");
            sql.append("messages.title as title, ");
            sql.append("messages.text as text, ");
            sql.append("users.login_id as login_id, ");
            sql.append("users.name as name, ");
            sql.append("users.id as user_id, ");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                String text = rs.getString("text");
                String categoly = rs.getString("categoly");
                int id = rs.getInt("id");
                String title = rs.getString("title");
                Timestamp createdDate = rs.getTimestamp("created_date");
                int userId = rs.getInt("user_id");
                String name = rs.getString("name");

                UserMessage message = new UserMessage();
                message.setText(text);
                message.setCategoly(categoly);
                message.setCreated_date(createdDate);
                message.setId(id);
                message.setTitle(title);
                message.setUserId(userId);
                message.setName(name);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}