package dao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.Blanch;


public class blanchdao {
	private static final String URL = "jdbc:mysql://localhost/egashira_fumito";
	private static final String USER = "root";
	private static final String PASSWORD = "root";
	private static final String DRIVER = "com.mysql.jdbc.Driver";

	static {
		try {
			Class.forName(DRIVER);
		}catch(ClassNotFoundException e){
			throw new RuntimeException(e);
		}
	}

	public ArrayList <Blanch> findall(){
		ArrayList<Blanch> blanchlist = new ArrayList<>();
		try (Connection con = DriverManager.getConnection(URL,USER,PASSWORD)){
			String sql = "SELECT id,blanch_name FROM blanches";
			PreparedStatement pstmt = con.prepareStatement(sql) ;
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				String blanch_name = rs.getString("blanch_name");
				Blanch blanch = new Blanch(id,blanch_name);
				blanchlist.add(blanch);
			}

		}catch(SQLException e){
			e.printStackTrace();
			return null;
		}
		return blanchlist;
	}
}
