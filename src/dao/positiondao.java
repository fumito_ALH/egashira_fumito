package dao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.Position;


public class positiondao {
	private static final String URL = "jdbc:mysql://localhost/egashira_fumito";
	private static final String USER = "root";
	private static final String PASSWORD = "root";
	private static final String DRIVER = "com.mysql.jdbc.Driver";

	static {
		try {
			Class.forName(DRIVER);
		}catch(ClassNotFoundException e){
			throw new RuntimeException(e);
		}
	}

	public ArrayList <Position> findall(){
		ArrayList<Position> positionlist = new ArrayList<>();
		try (Connection con = DriverManager.getConnection(URL,USER,PASSWORD)){
			String sql = "SELECT id,position_name FROM positions";
			PreparedStatement pstmt = con.prepareStatement(sql) ;
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				String position_name = rs.getString("position_name");
				Position position = new Position(id,position_name);
				positionlist.add(position);
			}

		}catch(SQLException e){
			e.printStackTrace();
			return null;
		}
		return positionlist;
	}
}
