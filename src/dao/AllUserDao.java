package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import beans.User;
import exception.SQLRuntimeException;


@WebServlet("/AllUserDao")
public class AllUserDao extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 public List<User> getUser(Connection connection) {
		 PreparedStatement ps = null;
		 ResultSet rs = null;
		 try {
	            String sql = "select users.login_id , users.name, users.pass, users.blanch , users.position , users.is_stopped , blanches.blanch_name , positions.position_name ,"
	            		+ " users.id , users.created_date , users.updated_date"
	            		+ " from users"
	            		+ " join blanches on users.blanch = blanches.id"
	            		+ " join positions on users.position = positions.id ;";

	            UserDao userd = new UserDao();
	            ps = connection.prepareStatement(sql);
	            rs = ps.executeQuery();
	            List<User> userList = userd.toUserList(rs);
	            return userList;
		 } catch (SQLRuntimeException | SQLException e) {
			 e.printStackTrace();
		 } finally {
			 close(rs);
			 close(ps);
		 }
	 return  null;
	 }
}
