package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Message;
import exception.SQLRuntimeException;

public class MessageDeleteDao {
	public void delete(Connection connection, Message message) {

	PreparedStatement ps = null;
    try {
        StringBuilder sql = new StringBuilder();
        sql.append("DELETE FROM messages WHERE id = ? ; ");
        ps = connection.prepareStatement(sql.toString());
        ps.setInt(1, message.getId());

        ps.executeUpdate();
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
}
}
