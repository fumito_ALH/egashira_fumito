package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("name");
            sql.append(", pass");
            sql.append(", login_id");
            sql.append(", blanch");
            sql.append(", position");
            sql.append(", is_stopped");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(", CURRENT_TIMESTAMP");
            sql.append(", CURRENT_TIMESTAMP");
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getName());
            ps.setString(2, user.getPass());
            ps.setString(3, user.getLogin_id());
            ps.setInt(4, user.getBlanch());
            ps.setInt(5, user.getPosition());
            ps.setInt(6, user.getIs_stopped());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public User getUser(Connection connection, String login_id, String pass) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT  * "
            		+ " FROM users"
            		+ " join blanches on blanches.id = users.blanch"
            		+ " join positions on positions.id = users.position"
            		+ " WHERE login_id = ? AND pass = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, login_id);
            ps.setString(2, pass);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
public List<User> toUserList(ResultSet rs) throws SQLException {

    List<User> ret = new ArrayList<User>();
    try {
        while (rs.next()) {
            String login_id = rs.getString("login_id");
            String pass = rs.getString("pass");
            String name = rs.getString("name");
            int blanch = rs.getInt("blanch");
            String blanchName = rs.getString("blanch_name");
            int position = rs.getInt("position");
            String positionName = rs.getString("position_name");
            int is_stopped = rs.getInt("is_stopped");
            int id = rs.getInt("id");
            Timestamp createdDate = rs.getTimestamp("created_date");
            Timestamp updatedDate = rs.getTimestamp("updated_date");
            User user = new User();
            user.setLogin_id(login_id);
            user.setPass(pass);
            user.setName(name);
            user.setBlanch(blanch);
            user.setBlanchName(blanchName);
            user.setPosition(position);
            user.setPositionName(positionName);
            user.setIs_stopped(is_stopped);
            user.setId(id);
            user.setCreatedDate(createdDate);
            user.setUpdatedDate(updatedDate);

            ret.add(user);
        }
        return ret;
    } finally {

        close(rs);
    }
}
public User getUser(Connection connection, int id) {

    PreparedStatement ps = null;
    try {
        String sql = "SELECT * from users"
        		+ " join blanches on blanches.id = users.blanch"
        		+ " join positions on positions.id = users.position"
        		+ " WHERE users.id = ?";

        ps = connection.prepareStatement(sql);
        ps.setInt(1, id);

        ResultSet rs = ps.executeQuery();
        List<User> userList = toUserList(rs);
        if (userList.isEmpty() == true) {
            return null;
        } else if (2 <= userList.size()) {
            throw new IllegalStateException("2 <= userList.size()");
        } else {
            return userList.get(0);
        }
    } catch (SQLException e) {
    	e.printStackTrace();
        return null;
    } finally {
        close(ps);
    }
}
public void update(Connection connection, User user) {

    PreparedStatement ps = null;
    try {
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE users SET");
        sql.append(" login_id  = ?");
        sql.append(", pass = ?");
        sql.append(", name = ?");
        sql.append(", blanch = ?");
        sql.append(", position = ?");
        sql.append(", is_stopped = ?");
        sql.append(", created_date = created_date");
        sql.append(", updated_date = current_timestamp");
        sql.append(" WHERE");
        sql.append(" id = ?");


        ps = connection.prepareStatement(sql.toString());

        java.util.Date utilDate = new java.util.Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(utilDate);
        cal.set(Calendar.MILLISECOND, 0);

        ps.setString(1, user.getLogin_id());
        ps.setString(2, user.getPass());
        ps.setString(3, user.getName());
        ps.setInt(4, user.getBlanch());
        ps.setInt(5, user.getPosition());
        ps.setInt(6, user.getIs_stopped());
        ps.setInt(7, user.getId());

        int count = ps.executeUpdate();
        if (count == 0) {
            throw new NoRowsUpdatedRuntimeException();
        }
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }

}

public void NPupdate(Connection connection, User user) {

    PreparedStatement ps = null;
    try {
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE users SET");
        sql.append(" login_id  = ?");
        sql.append(", name = ?");
        sql.append(", blanch = ?");
        sql.append(", position = ?");
        sql.append(", is_stopped = ?");
        sql.append(", updated_date = current_timestamp");
        sql.append(" WHERE");
        sql.append(" id = ?");


        ps = connection.prepareStatement(sql.toString());

        java.util.Date utilDate = new java.util.Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(utilDate);
        cal.set(Calendar.MILLISECOND, 0);

        ps.setString(1, user.getLogin_id());
        ps.setString(2, user.getName());
        ps.setInt(3, user.getBlanch());
        ps.setInt(4, user.getPosition());
        ps.setInt(5, user.getIs_stopped());
        ps.setInt(6, user.getId());

        int count = ps.executeUpdate();
        if (count == 0) {
            throw new NoRowsUpdatedRuntimeException();
        }
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }

}

public void stopdate(Connection connection, User user) {
	 PreparedStatement ps = null;
	    try {
	        StringBuilder sql = new StringBuilder();
	        sql.append("update users set is_stopped = ? where id = ?;");

	        ps = connection.prepareStatement(sql.toString());

	        ps.setInt(1, user.getIs_stopped());
	        ps.setInt(2, user.getId());

	        int count = ps.executeUpdate();
	        if (count == 0) {
	            throw new NoRowsUpdatedRuntimeException();
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
}
	}
