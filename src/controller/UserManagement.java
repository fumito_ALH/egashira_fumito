package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserService;

@WebServlet("/UserManagement")
public class UserManagement extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public void doGet (HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		this.doPost(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int editId = Integer.parseInt(request.getParameter("id"));
		User user = new UserService().getUser(editId);
		if(user.getIs_stopped() == 0) {
			//1にしたい
			user.setIs_stopped(1);
		}else {
			user.setIs_stopped(0);
		}
		new UserService().stopdate(user);
		response.sendRedirect("AllUser");
	}

}
