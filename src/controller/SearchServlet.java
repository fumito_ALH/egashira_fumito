package controller;

import java.io.IOException;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserMessage;
import service.SearchService;

@WebServlet("/SearchServlet")
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String strDate = request.getParameter("Sdate");
		String intDate = request.getParameter("Edate");

		if (strDate == "" && intDate == "") {
				request.getRequestDispatcher("index.jsp").forward(request, response);
				return;
		}
		if(strDate != "" || intDate != "") {
			if(strDate == "") {
				strDate = "2000-01-01";
			}
			if(intDate == "") {
				intDate = "3000-01-01";
			}

		Date searchDate = java.sql.Date.valueOf(strDate);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(searchDate);
		calendar.add(Calendar.DATE, 1);

		Date searchAfterDate = java.sql.Date.valueOf(intDate);
		SearchService serchservice = new SearchService();
		List<UserMessage> nosa = serchservice.getUserMessages(searchDate, searchAfterDate);
		HttpSession session = request.getSession();
		session.setAttribute("messages", nosa);

		request.getRequestDispatcher("/top.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
