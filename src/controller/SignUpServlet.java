package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Blanch;
import beans.Position;
import beans.User;
import dao.blanchdao;
import dao.positiondao;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		positiondao hensu = new positiondao();
		ArrayList<Position> position = hensu.findall();
		request.setAttribute("position", position);

		blanchdao hensuu = new blanchdao();
		ArrayList<Blanch> blanch = hensuu.findall();
		request.setAttribute("blanch", blanch);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();
		if (isValid(request, messages) == true) {

			User user = new User();
			user.setName(request.getParameter("name"));
			user.setLogin_id(request.getParameter("login_id"));
			user.setPass(request.getParameter("pass"));
			user.setBlanch(Integer.parseInt(request.getParameter("blanch")));
			user.setPosition(Integer.parseInt(request.getParameter("position")));

			new UserService().register(user);

			response.sendRedirect("AllUser");
			return;
		}
		session.setAttribute("errorMessages", messages);
		response.sendRedirect("signup");
		return;

	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String name = request.getParameter("name");
		String pass = request.getParameter("pass");
		String repass = request.getParameter("repass");
		String login_id = request.getParameter("login_id");
		int blanch = Integer.parseInt(request.getParameter("blanch"));
		int position = Integer.parseInt(request.getParameter("position"));

		if (StringUtils.isEmpty(login_id) == true) { //StringUtilsでは正規表現が使えない可能性
			messages.add("ログインIDを入力してください");
		} else if (!(login_id.matches("[0-9a-zA-Z]{6,20}"))) {
			messages.add("ログインIDは半角英数字6文字以上20文字以内で入力してください");
		}
		if (StringUtils.isEmpty(name) == true) { //StringUtilsでは正規表現が使えない可能性
			messages.add("名前を入力してください");
		}
		if (StringUtils.length(name) > 10) {
			messages.add("名前は10文字以内で入力してください");
		}
		if (StringUtils.isEmpty(pass) == true) {
			messages.add("パスワードを入力してください");
		} else if (!(pass.matches("[0-9a-zA-Z]{6,20}"))) {
			messages.add("パスワードは半角英数字6文字以上20文字以内で入力してください");
		} else if (!(pass.equals(repass))) {
			messages.add("パスワードと確認用パスワードが違います");
		}
		if(blanch == 2 && position == 1) {
			messages.add("役職と支店の組み合わせが正しくありません");
		}
		if(blanch == 3 && position == 1) {
			messages.add("役職と支店の組み合わせが正しくありません");
		}
		if(blanch == 2 && position == 2) {
			messages.add("役職と支店の組み合わせが正しくありません");
		}
		if(blanch == 3 && position == 2) {
			messages.add("役職と支店の組み合わせが正しくありません");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}