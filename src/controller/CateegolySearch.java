package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserMessage;
import service.SearchService;

@WebServlet("/CateegolySearch")
public class CateegolySearch extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String catee = request.getParameter("search");
		if (catee == null || catee == "") {
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		SearchService serchservice = new SearchService();
		List<UserMessage> nosa = serchservice.getUserMessages(catee);

		HttpSession session = request.getSession();
		session.setAttribute("messages", nosa);

		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
