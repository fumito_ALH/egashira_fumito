package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import beans.UserMessage;
import service.MessageService;

@WebServlet(urlPatterns = { "/newMessage" })

public class NewMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<UserMessage> messages = new MessageService().getMessage();
		HttpSession session = request.getSession();
		session.setAttribute("messages", messages);

		request.getRequestDispatcher("/post.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		if (isValid(request, messages) == true) {

			User user = (User) session.getAttribute("loginUser");

			Message message = new Message();
			message.setCategoly(request.getParameter("categoly"));
			message.setTitle(request.getParameter("title"));
			message.setText(request.getParameter("text"));
			message.setUser_id(user.getId());

			new MessageService().register(message);

			response.sendRedirect("index.jsp");
		} else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("newMessage");
			return;
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String message = request.getParameter("text");
		String categoly = request.getParameter("categoly");
		String title = request.getParameter("title");

		if (StringUtils.isEmpty(message) == true) {
			messages.add("本文を入力してください");
		}
		if (StringUtils.isEmpty(categoly) == true) {
			messages.add("カテゴリーを入力してください");
		}
		if (StringUtils.isEmpty(title) == true) {
			messages.add("タイトルを入力してください");
		}
		if(10 < categoly.length()) {
			messages.add("カテゴリーは10文字以下で入力してください");
		}
		if(30 < title.length()) {
			messages.add("タイトルは30文字以下で入力してください");
		}
		if (1000 < message.length()) {
			messages.add("本文は1000文字以下で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
