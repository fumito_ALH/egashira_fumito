package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = {"/NewComment" })

public class NewCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("loginUser");
        request.getParameter("messages");


        Comment comment = new Comment();
        comment.setText(request.getParameter("text"));
        comment.setUser_id(user.getId());
        comment.setMessage_id(Integer.parseInt(request.getParameter("messages")));
        List<String> messages = new ArrayList<String>();

        if (isValid(messages, comment) == true) {

            new CommentService().register(comment);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("./");
        }
    }
	private boolean isValid(List<String> messages,Comment comments) {

		if (StringUtils.isEmpty(comments.getText()) == true) {
			messages.add("メッセージを入力してください");
		}
		if (500 < comments.getText().length()) {
			messages.add("500文字以下で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
