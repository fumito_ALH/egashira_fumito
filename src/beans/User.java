package beans;

import java.io.Serializable;
import java.sql.Timestamp;

public class User implements Serializable {
	private static final long serialVersionUID = 1L;

        private int id;
        private int blanch;
        private String blanchName;
        private int position;
        private String positionName;
        private int is_stopped;
        private String login_id;
        private String name;
        private String pass;
        private String repass;

		private Timestamp createdDate;
        private Timestamp updatedDate;
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public int getBlanch() {
			return blanch;
		}
		public void setBlanch(int blanch) {
			this.blanch = blanch;
		}
		public int getPosition() {
			return position;
		}
		public void setPosition(int position) {
			this.position = position;
		}
		public int getIs_stopped() {
			return is_stopped;
		}
		public void setIs_stopped(int is_stopped) {
			this.is_stopped = is_stopped;
		}
		public String getLogin_id() {
			return login_id;
		}
		public void setLogin_id(String login_id) {
			this.login_id = login_id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getPass() {
			return pass;
		}
		public void setPass(String pass) {
			this.pass = pass;
		}
        public String getRepass() {
			return repass;
		}
		public void setRepass(String repass) {
			this.repass = repass;
		}
		public Timestamp getCreatedDate() {
			return createdDate;
		}
		public void setCreatedDate(Timestamp createdDate) {
			this.createdDate = createdDate;
		}
		public Timestamp getUpdatedDate() {
			return updatedDate;
		}
		public void setUpdatedDate(Timestamp updatedDate) {
			this.updatedDate = updatedDate;
		}
		public String getBlanchName() {
			return blanchName;
		}
		public void setBlanchName(String blanchName) {
			this.blanchName = blanchName;
		}
		public String getPositionName() {
			return positionName;
		}
		public void setPositionName(String positionName) {
			this.positionName = positionName;
		}

}