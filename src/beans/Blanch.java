package beans;

import java.io.Serializable;

public class Blanch implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String blanch_name;

	public Blanch(int id, String blanch_name) {
		this.id = id;
		this.blanch_name = blanch_name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBlanch_name() {
		return blanch_name;
	}
	public void setBlanch_name(String blanch_name) {
		this.blanch_name = blanch_name;
	}

}
