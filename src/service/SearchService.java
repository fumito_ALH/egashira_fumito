package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.sql.Date;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import beans.UserMessage;
import dao.SearchDao;

@WebServlet("/SearchService")
public class SearchService extends HttpServlet {
	private static final long serialVersionUID = 1L;
		public List<UserMessage> getUserMessages(Date searchDate ,Date searchAfterDate ) {
			 Connection connection = null;
			    try {
			        connection = getConnection();
			        SearchDao SearchDao = new SearchDao();
			        List<UserMessage> userd = SearchDao.getUserMessages(connection,searchDate,searchAfterDate);
			        return userd;
			    } catch (RuntimeException e) {
			        rollback(connection);
			        throw e;
			    } catch (Error e) {
			        rollback(connection);
			        throw e;
			    } finally {
			        close(connection);
			    }
		}
		public List<UserMessage> getUserMessages(String catee) {
			 Connection connection = null;
			    try {
			        connection = getConnection();
			        SearchDao SearchDao = new SearchDao();
			        List<UserMessage> userd = SearchDao.getUserMessages(connection,catee);
			        return userd;
			    } catch (RuntimeException e) {
			        rollback(connection);
			        throw e;
			    } catch (Error e) {
			        rollback(connection);
			        throw e;
			    } finally {
			        close(connection);
			    }
		}

}
