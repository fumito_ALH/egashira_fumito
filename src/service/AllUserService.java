package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import beans.User;
import dao.AllUserDao;

@WebServlet("/AllUserService")
public class AllUserService extends HttpServlet {
	private static final long serialVersionUID = 1L;
		public List<User> getUser() {
			 Connection connection = null;
			    try {
			        connection = getConnection();
			        AllUserDao userDao = new AllUserDao();
			        List<User> userd = userDao.getUser(connection);
			        return userd;
			    } catch (RuntimeException e) {
			        rollback(connection);
			        throw e;
			    } catch (Error e) {
			        rollback(connection);
			        throw e;
			    } finally {
			        close(connection);
			    }
		}

}
